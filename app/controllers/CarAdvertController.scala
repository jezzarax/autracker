package controllers

import dal.CarAdvertRepositoryComponent
import domain.{CarAdvert, FuelType}
import infrastructure.ProductionAppConfiguration
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.data.validation.ValidationError
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc._

import scala.util.{Failure, Success, Try}


trait CarAdvertControllerComponent extends Controller {
  this: CarAdvertRepositoryComponent =>

  implicit val fuelTypeJsonFormat = new Format[FuelType.Value] {
    override def writes(fuelType: FuelType.Value): JsValue = JsString(fuelType.toString)

    override def reads(json: JsValue): JsResult[FuelType.Value] = {
      if (FuelType.values.map(_.toString).contains(json.as[String])) {
        JsSuccess(FuelType.withName(json.as[String]))
      }
      else {
        JsError(ValidationError(s"Unknown fuel type ${json.as[String]}"))
      }
    }
  }

  implicit val ISO8601DateJsonFormat = new Format[DateTime] {
    override def writes(date: DateTime): JsValue = JsString(date.toString(DateTimeFormat.forPattern("yyyy-MM-dd'T00:00:00'Z")))

    override def reads(json: JsValue): JsResult[DateTime] = {
      Try({
        DateTime.parse(json.as[String], DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ"))
      }) match {
        case Success(parsedDateTime) => JsSuccess(parsedDateTime)
        case Failure(e) => JsError(ValidationError(s"cannot parse the date, expected format yyyy-MM-ddTHH:mm:ssZ. Exception $e"))
      }
    }
  }

  implicit val carAdvertReader: Reads[CarAdvert] = (
    (__ \ "id").read[String]
      and (__ \ "title").read[String]
      and (__ \ "price").read[Int]
      and (__ \ "fuelType").read[FuelType.Value]
      and (__ \ "isNew").read[Boolean]
      and (__ \ "mileage").readNullable[Int]
      and (__ \ "firstRegistrationDate").readNullable[DateTime]

    ) (CarAdvert)
  implicit val carAdvertWriter = Json.writes[CarAdvert]
  implicit val validationErrorWriter = Json.writes[ValidationErrorDescription]

  def getCarAdvertById(id: String): Action[AnyContent] = Action {
    carAdvertRepository.findOneById(id) match {
      case Some(advert) => Ok(Json.toJson(advert))
      case None => NotFound("No car advert with such id")
    }
  }

  def listAllAdverts(orderingField: Option[String], ascending: Option[String]): Action[AnyContent] = Action {
    orderingField match {
      case None => Ok (Json.toJson (carAdvertRepository.getAll) )
      case Some(field) => {
        ascending.getOrElse("false").trim.toLowerCase match {
          case "true" => Ok (Json.toJson (carAdvertRepository.getAllSortedBy(field, ascendingSort = true)) )
          case _ => Ok (Json.toJson (carAdvertRepository.getAllSortedBy(field, ascendingSort = false)) )
        }
      }
    }
  }

  def createCarAdvert: Action[AnyContent] = Action {
    request =>
      processRepositoryAction(request.body.asJson, carAdvertRepository.insert, v => Created(v))
  }

  def updateCarAdvert: Action[AnyContent] = Action {
    request => processRepositoryAction(request.body.asJson, carAdvertRepository.update, v => Ok(v))
  }

  def processRepositoryAction(jsonBody: Option[JsValue], action: CarAdvert => CarAdvert, success: JsValue => Result): Result = {
    jsonBody match {
      case Some(jsonBlock) =>
        val parsedAdvert = Json.fromJson[CarAdvert](jsonBlock)
        parsedAdvert match {
          case JsSuccess(advert, _) =>
            customCarAdvertValidation(advert) match {
              case None => success(Json.toJson(action(advert)))
              case Some(errors) => NotAcceptable(Json.toJson(errors))
            }

          case JsError(errors) => NotAcceptable(Json.toJson(errors.flatMap { e => e._2.map(ve => ValidationErrorDescription(e._1.toJsonString, ve.message)) }))
        }
      case None => NotAcceptable
    }
  }

  def customCarAdvertValidation(advert: CarAdvert): Option[Seq[ValidationErrorDescription]] = {
    if (advert.isNew) {
      None
    }
    else {
      val errors = new scala.collection.mutable.MutableList[ValidationErrorDescription]()
      if (!advert.mileage.isDefined)
        errors += ValidationErrorDescription("mileage", "Mileage must be present for used cars")
      if (!advert.firstRegistrationDate.isDefined)
        errors += ValidationErrorDescription("firstRegistrationDate", "First registration date must be present for used cars")
      if (errors.count(p => true) > 0) {
        Some(errors)
      } else {
        None
      }
    }

  }

  def deleteCarAdvertById(advertId: String): Action[AnyContent] = Action {
    carAdvertRepository.delete(advertId) match {
      case Some(deletedAdvertId) => Ok(deletedAdvertId)
      case None => NotFound("No advert with such Id")
    }
  }

  case class ValidationErrorDescription(path: String, errorDescription: String)
}


object CarAdvertController extends CarAdvertControllerComponent
  with CarAdvertRepositoryComponent
  with ProductionAppConfiguration {
  val carAdvertRepository = new CarAdvertRepository

}
