package infrastructure

import com.typesafe.config.ConfigFactory

/**
  * Created by alexey.kuntsevich on 07.09.2016.
  */
trait AppConfiguration {

  val connectionString: String
  val databaseName: String
  val timeoutDuration: Int = 10
}

trait ProductionAppConfiguration extends AppConfiguration {
  val config = ConfigFactory.load
  val connectionString = config.getString("database.connectionString")
  val databaseName = config.getString("database.name")
  override val timeoutDuration: Int = 5
}
