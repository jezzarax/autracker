package dal


import java.util.concurrent.TimeUnit

import domain.{CarAdvert, FuelType}
import infrastructure.AppConfiguration
import org.bson.conversions.Bson
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, ISODateTimeFormat}
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.Updates.{combine, set, unset}
import org.mongodb.scala.result._
import org.mongodb.scala.model.Sorts.{ascending, descending}
import org.mongodb.scala.{Completed, Document, MongoClient, MongoCollection, MongoDatabase, Observable}

import scala.concurrent.Await
import scala.concurrent.duration.Duration


trait CarAdvertRepositoryComponent {
  this: AppConfiguration =>
  val carAdvertRepository: CarAdvertRepository

  trait ImplicitObservable[C] {
    val observable: Observable[C]
    val converter: (C) => String

    def results(): Seq[C] = Await.result(observable.toFuture(), Duration(timeoutDuration, TimeUnit.SECONDS))
  }

  implicit class DocumentObservable[C](val observable: Observable[Document]) extends ImplicitObservable[Document] {
    override val converter: (Document) => String = (doc) => doc.toJson
  }

  implicit class CompletedObservable[C](val observable: Observable[Completed]) extends ImplicitObservable[Completed] {
    override val converter: (Completed) => String = (obs) => obs.toString
  }

  implicit class UpdatedObservable[C](val observable: Observable[UpdateResult]) extends ImplicitObservable[UpdateResult] {
    override val converter: (UpdateResult) => String = (obs) => obs.toString
  }

  implicit class DeletedObservable[C](val observable: Observable[DeleteResult]) extends ImplicitObservable[DeleteResult] {
    override val converter: (DeleteResult) => String = (obs) => obs.toString
  }

  class CarAdvertRepository {

    val mongoClient: MongoClient = MongoClient(connectionString)
    val database: MongoDatabase = mongoClient.getDatabase(databaseName)
    val collection: MongoCollection[Document] = database.getCollection("car_adverts")
    val dateParser = ISODateTimeFormat.dateTimeNoMillis()

    implicit def carAdvertToDocument(c: CarAdvert): Document = {
      if (c.isNew) {
        Document(
          "_id" -> c.id,
          "title" -> c.title,
          "price" -> c.price,
          "fuelType" -> c.fuelType.toString,
          "isNew" -> true,
          "mileage" -> None,
          "firstRegistrationDate" -> None
        )
      } else {
        Document(
          "_id" -> c.id,
          "title" -> c.title,
          "price" -> c.price,
          "fuelType" -> c.fuelType.toString,
          "isNew" -> c.isNew,
          "mileage" -> c.mileage.get,
          "firstRegistrationDate" -> c.firstRegistrationDate.get.toString(DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ"))
        )
      }
    }

    def insert(advert: CarAdvert): CarAdvert = {
      val uuid = java.util.UUID.randomUUID.toString
      collection.insertOne(advert.copy(id = uuid)).results()
      advert.copy(id = uuid)
    }

    def findOneById(id: String): Option[CarAdvert] = {
      val searchResults = collection.find(equal("_id", id)).results()
      searchResults.count(p => true) match {
        case 1 => Some(searchResults.head)
        case _ => None

      }
    }

    def getAll: Seq[CarAdvert] = collection.find().results().map(documentToCarAdvert)

    def getAllSortedBy(field: String, ascendingSort: Boolean): Seq[CarAdvert] =
      collection.find()
        .sort(if(ascendingSort) {ascending(field)} else {descending(field)})
        .results().map(documentToCarAdvert)

    implicit def documentToCarAdvert(d: Document): CarAdvert = {
      if (d.get("isNew").get.asBoolean().getValue) {
        CarAdvert(
          d.get("_id").get.asString().getValue,
          d.get("title").get.asString().getValue,
          d.get("price").get.asInt32().getValue,
          FuelType.withName(d.get("fuelType").get.asString().getValue),
          isNew = true,
          None,
          None

        )
      } else {
        CarAdvert(
          d.get("_id").get.asString().getValue,
          d.get("title").get.asString().getValue,
          d.get("price").get.asInt32().getValue,
          FuelType.withName(d.get("fuelType").get.asString().getValue),
          isNew = false,
          Some(d.get("mileage").get.asInt32().getValue),
          Some(DateTime.parse(
            d.get("firstRegistrationDate").get.asString().getValue,
            DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ")
          ))
        )
      }

    }

    def update(advert: CarAdvert): CarAdvert = {
      collection.updateOne(equal("_id", advert.id), carAdvertToUpdateDocument(advert)).results().head.getMatchedCount match {
        case 1 => advert
        case _ => {
          collection.insertOne(advert).results()
          advert
        }
      }

    }

    def carAdvertToUpdateDocument(c: CarAdvert): Bson = {
      if (c.isNew) {
        combine(
          set("title", c.title),
          set("price", c.price),
          set("fuelType", c.fuelType.toString),
          set("isNew", true),
          unset("mileage"),
          unset("firstRegistrationDate")
        )
      } else {
        combine(
          set("title", c.title),
          set("price", c.price),
          set("fuelType", c.fuelType.toString),
          set("isNew", false),
          set("mileage", c.mileage.get),
          set("firstRegistrationDate", c.firstRegistrationDate.get.toString(DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ")))
        )
      }
    }

    def delete(advertId: String): Option[String] = {
      val deleteResult = collection.deleteOne(equal("_id", advertId)).results()
      deleteResult.head.getDeletedCount match {
        case 1 => Some(advertId)
        case _ => None
      }
    }
  }

}
