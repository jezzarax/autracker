package domain

object FuelType extends Enumeration {
  val gasoline = Value("gasoline")
  var diesel = Value("diesel")
}
