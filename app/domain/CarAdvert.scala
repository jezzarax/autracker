package domain


import org.joda.time.DateTime



case class CarAdvert(
                      id: String,
                      title: String,
                      price: Int,
                      fuelType: FuelType.Value,
                      isNew: Boolean,
                      mileage: Option[Int],
                      firstRegistrationDate: Option[DateTime]

                    )

