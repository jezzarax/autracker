import java.util.concurrent.TimeUnit

import dal.CarAdvertRepositoryComponent
import domain.{CarAdvert, FuelType}
import infrastructure.AppConfiguration
import org.joda.time.DateTime
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.{Document, MongoClient, MongoDatabase, Observable}
import org.scalatest.BeforeAndAfter
import org.scalatestplus.play.PlaySpec

import scala.concurrent.Await
import scala.concurrent.duration.Duration


trait TestAppConfiguration extends AppConfiguration {
  override val connectionString: String = "mongodb://localhost"
  override val databaseName: String = "autracker_tests"
}

class RepositorySpec extends PlaySpec with BeforeAndAfter {

  val testAdvert = CarAdvert("", "Audi Ar", 10, FuelType.diesel, true, None, None)
  val testUsedCarAdvert = CarAdvert("", "Mustang", 20, FuelType.gasoline, false, Some(20000), Some(new DateTime("2001-01-01")))
  val advertsCollectionName = "car_adverts"

  before {
    DatabaseTestHelper.wipeData()
  }

  "CarAdvertRepository" should {
    object componentRegistry extends CarAdvertRepositoryComponent with TestAppConfiguration {
      val carAdvertRepository = new CarAdvertRepository
    }
    "create a document on insertion" in {
      val insertedAd = componentRegistry.carAdvertRepository.insert(testAdvert)
      DatabaseTestHelper.ensureObjectExists(advertsCollectionName, insertedAd.id) mustBe true
    }

    "return existing document or none" in {
      val insertedAd = componentRegistry.carAdvertRepository.insert(testAdvert)
      val documentFromDatabase = componentRegistry.carAdvertRepository.findOneById(insertedAd.id)
      documentFromDatabase mustBe Some(testAdvert.copy(id = insertedAd.id))

      componentRegistry.carAdvertRepository.findOneById("fake id") mustBe None
    }

    "list all created adverts" in {
      componentRegistry.carAdvertRepository.getAll mustBe Seq[CarAdvert]()
      componentRegistry.carAdvertRepository.insert(testAdvert)
      componentRegistry.carAdvertRepository.insert(testAdvert.copy(title = "Mustang"))

      val advertListAfterInsertions = componentRegistry.carAdvertRepository.getAll
      advertListAfterInsertions.map(x => x.title).sortBy(t => t) mustBe Seq[String]("Audi Ar", "Mustang")
    }

    "update existing advert" in {
      val insertedAdId = componentRegistry.carAdvertRepository.insert(testAdvert).id
      val updatedAd = testAdvert.copy(id = insertedAdId, title = "Mustang", price = 20, fuelType = FuelType.gasoline)
      val updateResult = componentRegistry.carAdvertRepository.update(updatedAd)
      updateResult mustBe updatedAd
      val collectionDocuments = DatabaseTestHelper.getCollectionDocuments(advertsCollectionName)

      collectionDocuments.count(p => true) mustBe 1
      collectionDocuments.head.get("title").get.asString().getValue mustBe "Mustang"
      collectionDocuments.head.get("price").get.asInt32().getValue mustBe 20
      collectionDocuments.head.get("fuelType").get.asString().getValue mustBe "gasoline"

    }

    "update of non-existent advert should create one" in {
      val updateResult = componentRegistry.carAdvertRepository.update(testAdvert)
      DatabaseTestHelper.ensureObjectExists(advertsCollectionName, testAdvert.id) mustBe true
    }

    "delete existing advert" in {
      val insertedAdId = componentRegistry.carAdvertRepository.insert(testAdvert).id
      val deleteResult = componentRegistry.carAdvertRepository.delete(insertedAdId)
      deleteResult mustBe Some(insertedAdId)
      DatabaseTestHelper.ensureObjectExists(advertsCollectionName, insertedAdId) mustBe false

      componentRegistry.carAdvertRepository.delete("fakeId") mustBe None
    }

    "create and delete used car advert" in {
      val insertedAdId = componentRegistry.carAdvertRepository.insert(testUsedCarAdvert).id
      DatabaseTestHelper.ensureObjectExists(advertsCollectionName, insertedAdId) mustBe true
      val insertedDocument = DatabaseTestHelper.getCollectionDocumentById(advertsCollectionName, insertedAdId)

      insertedDocument.get("mileage").get.asInt32().getValue mustBe 20000
      insertedDocument.get("firstRegistrationDate").get.asString().getValue mustBe "2001-01-01T00:00:00+0100"

      componentRegistry.carAdvertRepository.delete(insertedAdId)

      DatabaseTestHelper.ensureObjectExists(advertsCollectionName, insertedAdId) mustBe false

    }

    "update car advert from used to new and back" in {
      val insertedAdId = componentRegistry.carAdvertRepository.insert(testUsedCarAdvert).id
      DatabaseTestHelper.ensureObjectExists(advertsCollectionName, insertedAdId) mustBe true

      componentRegistry.carAdvertRepository.update(testUsedCarAdvert.copy(id = insertedAdId, isNew = true))

      val updatedDocument = DatabaseTestHelper.getCollectionDocumentById(advertsCollectionName, insertedAdId)

      updatedDocument.get("mileage") mustBe None
      updatedDocument.get("firstRegistrationDate") mustBe None

      componentRegistry.carAdvertRepository.update(testUsedCarAdvert.copy(id = insertedAdId)).id

      val updatedNewDocument = DatabaseTestHelper.getCollectionDocumentById(advertsCollectionName, insertedAdId)

      updatedNewDocument.get("mileage").get.asInt32().getValue mustBe 20000
      updatedNewDocument.get("firstRegistrationDate").get.asString().getValue mustBe "2001-01-01T00:00:00+0100"

    }
  }
}

object DatabaseTestHelper extends TestAppConfiguration {

  val mongoClient: MongoClient = MongoClient(connectionString)
  val database: MongoDatabase = mongoClient.getDatabase(databaseName)

  def wipeData(): Unit = getResult(database.drop())

  def ensureObjectExists(collectionName: String, id: String): Boolean =
    getResult(database.getCollection[Document](collectionName).find(equal("_id", id))).count(p => true) == 1

  def getResult[A](observable: Observable[A]): Seq[A] = Await.result(observable.toFuture(), Duration(timeoutDuration, TimeUnit.SECONDS))

  def getCollectionDocuments(collectionName: String): Seq[Document] =
    getResult(database.getCollection(collectionName).find())

  def getCollectionDocumentById(collectionName: String, id: String): Document =
    getResult(database.getCollection(collectionName).find(equal("_id", id))).head

}
