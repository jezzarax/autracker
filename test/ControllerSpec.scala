import controllers.CarAdvertControllerComponent
import dal.CarAdvertRepositoryComponent
import domain.{CarAdvert, FuelType}
import org.joda.time.DateTime
import org.scalamock.scalatest.MockFactory
import org.scalatestplus.play._
import play.api.test.Helpers._
import play.api.test._


class ControllerSpec extends PlaySpec with OneAppPerTest with MockFactory {

  "CarAdvertController" should {

    "send 404 on a bad request" in {
      route(app, FakeRequest(GET, "/boum")).map(status) mustBe Some(NOT_FOUND)
    }

    val testAdvert1 = CarAdvert("1", "Audi A4", 10, FuelType.diesel, true, None, None)
    val testAdvert2 = CarAdvert("2", "Mustang", 20, FuelType.gasoline, true, None, None)
    val testAdvert1WithEmptyId = CarAdvert("", "Audi A4", 10, FuelType.diesel, true, None, None)
    val testAdvert1Json = """{"id":"1","title":"Audi A4","price":10,"fuelType":"diesel","isNew":true}"""
    val testAdvert1WithEmptyIdJson = """{"id":"","title":"Audi A4","price":10,"fuelType":"diesel","isNew":true}"""
    val testAdvert2Json = """{"id":"2","title":"Mustang","price":20,"fuelType":"gasoline","isNew":true}"""
    val testAdvertUsedCar1 = CarAdvert(
      "3",
      "ZAZ-968",
      5,
      FuelType.gasoline,
      isNew = false,
      Some(100000),
      Some(new DateTime("1976-12-25"))
    )
    val testAdvertUsedCar1WithEmptyId = CarAdvert(
      "",
      "ZAZ-968",
      5,
      FuelType.gasoline,
      isNew = false,
      Some(100000),
      Some(new DateTime("1976-12-25"))
    )
    val testAdvertUsedCar1Json = """{"id":"3","title":"ZAZ-968","price":5,"fuelType":"gasoline","isNew":false,""" +
      """"mileage":100000,"firstRegistrationDate":"1976-12-25T00:00:00+0100"}"""
    val testAdvertUsedCar1WithEmptyIdJson = """{"id":"","title":"ZAZ-968","price":5,"fuelType":"gasoline",""" +
      """"isNew":false,"mileage":100000,"firstRegistrationDate":"1976-12-25T00:00:00+0100"}"""


    "return correct json result based on object provided by the carService" in {

      object carAdvertController extends CarAdvertControllerComponent
        with CarAdvertRepositoryComponent
        with TestAppConfiguration {
        val carAdvertRepositoryMock = mock[CarAdvertRepository]
        (carAdvertRepositoryMock.findOneById _).expects("1").returning(Some(testAdvert1)).once
        (carAdvertRepositoryMock.findOneById _).expects("2").returning(None).once
        (carAdvertRepositoryMock.insert _).expects(testAdvert1WithEmptyId).returning(testAdvert1).anyNumberOfTimes
        val carAdvertRepository = carAdvertRepositoryMock

      }


      val resultForNonExistingId = call(carAdvertController.getCarAdvertById("2"), FakeRequest())
      contentAsString(resultForNonExistingId) mustBe "No car advert with such id"
      status(resultForNonExistingId) mustBe 404
      val resultForExistingId = call(carAdvertController.getCarAdvertById("1"), FakeRequest())
      status(resultForExistingId) mustBe 200
      contentAsString(resultForExistingId) mustBe testAdvert1Json
    }

    "fail with HTTP NotAcceptable or nonparseable or empty post and provide validation errors" in {

      object carAdvertController extends CarAdvertControllerComponent
        with CarAdvertRepositoryComponent
        with TestAppConfiguration {
        val carAdvertRepository = mock[CarAdvertRepository]
      }

      status(call(carAdvertController.createCarAdvert, FakeRequest())) mustBe 406
      val invalidRequest = call(carAdvertController.createCarAdvert,
        FakeRequest(
          Helpers.POST,
          controllers.routes.CarAdvertController.createCarAdvert().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          """ {"notId": "_"} """))

      status(invalidRequest) mustBe 406
      contentAsString(invalidRequest) mustBe
        """[{"path":"obj.id","errorDescription":"error.path.missing"},{"path":"obj""" +
          """.title","errorDescription":"error.path.missing"},{"path":"obj.isNew",""" +
          """"errorDescription":"error.path.missing"},{"path":"obj.fuelType","errorDescription"""" +
          """:"error.path.missing"},{"path":"obj.price","errorDescription":"error.path.missing"}]"""
    }

    "create a car advert on a valid request" in {

      object carAdvertController extends CarAdvertControllerComponent
        with CarAdvertRepositoryComponent
        with TestAppConfiguration {
        val carAdvertRepositoryMock = mock[CarAdvertRepository]
        (carAdvertRepositoryMock.insert _).expects(testAdvert1WithEmptyId).returning(testAdvert1).anyNumberOfTimes
        val carAdvertRepository = carAdvertRepositoryMock
      }

      val creationCallResult = call(carAdvertController.createCarAdvert,
        FakeRequest(
          Helpers.POST,
          controllers.routes.CarAdvertController.createCarAdvert().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          testAdvert1WithEmptyIdJson))
      status(creationCallResult) mustBe 201
      contentAsString(creationCallResult) mustBe testAdvert1Json
    }

    "return all car adverts" in {

      object carAdvertController extends CarAdvertControllerComponent
        with CarAdvertRepositoryComponent
        with TestAppConfiguration {
        val repositoryMock = mock[CarAdvertRepository]
        (repositoryMock.getAll _).expects().returning(Seq(testAdvert1, testAdvert2))
        val carAdvertRepository = repositoryMock
      }

      val listingCallResult = call(carAdvertController.listAllAdverts(None, None), FakeRequest())

      status(listingCallResult) mustBe 200
      contentAsString(listingCallResult) mustBe s"[$testAdvert1Json,$testAdvert2Json]"
    }

    "update car advert" in {
      object carAdvertController extends CarAdvertControllerComponent
        with CarAdvertRepositoryComponent
        with TestAppConfiguration {
        val repositoryMock = mock[CarAdvertRepository]
        (repositoryMock.update _).expects(testAdvert1).returning(testAdvert1).once
        val carAdvertRepository = repositoryMock
      }

      val updateCallResult = call(carAdvertController.updateCarAdvert,
        FakeRequest(
          Helpers.PUT,
          controllers.routes.CarAdvertController.updateCarAdvert().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          testAdvert1Json))

      status(updateCallResult) mustBe 200
      contentAsString(updateCallResult) mustBe testAdvert1Json
    }

    "delete car advert" in {
      object carAdvertController extends CarAdvertControllerComponent
        with CarAdvertRepositoryComponent
        with TestAppConfiguration {
        val repositoryMock = mock[CarAdvertRepository]
        (repositoryMock.delete _).expects("1").returning(Some("1")).once
        (repositoryMock.delete _).expects("2").returning(None).once
        val carAdvertRepository = repositoryMock
      }

      val deleteCallResult = call(carAdvertController.deleteCarAdvertById("1"),
        FakeRequest(
          Helpers.DELETE,
          controllers.routes.CarAdvertController.deleteCarAdvertById("1").url))

      status(deleteCallResult) mustBe 200
      contentAsString(deleteCallResult) mustBe "1"

      val failedUpdateCallResult = call(carAdvertController.deleteCarAdvertById("2"),
        FakeRequest(
          Helpers.DELETE,
          controllers.routes.CarAdvertController.deleteCarAdvertById("2").url))

      status(failedUpdateCallResult) mustBe 404
      contentAsString(failedUpdateCallResult) mustBe """No advert with such Id"""
    }

    "create a used car advert on a valid request" in {

      object carAdvertController extends CarAdvertControllerComponent
        with CarAdvertRepositoryComponent
        with TestAppConfiguration {
        val carAdvertRepositoryMock = mock[CarAdvertRepository]
        (carAdvertRepositoryMock.insert _).expects(testAdvertUsedCar1WithEmptyId).returning(testAdvertUsedCar1).anyNumberOfTimes
        val carAdvertRepository = carAdvertRepositoryMock
      }



      val creationCallResult = call(carAdvertController.createCarAdvert,
        FakeRequest(
          Helpers.POST,
          controllers.routes.CarAdvertController.createCarAdvert().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          testAdvertUsedCar1WithEmptyIdJson))
      status(creationCallResult) mustBe 201
      contentAsString(creationCallResult) mustBe testAdvertUsedCar1Json
    }

    "update used car advert" in {
      object carAdvertController extends CarAdvertControllerComponent
        with CarAdvertRepositoryComponent
        with TestAppConfiguration {
        val repositoryMock = mock[CarAdvertRepository]
        (repositoryMock.update _).expects(testAdvertUsedCar1).returning(testAdvertUsedCar1).once
        val carAdvertRepository = repositoryMock
      }

      val updateCallResult = call(carAdvertController.updateCarAdvert,
        FakeRequest(
          Helpers.PUT,
          controllers.routes.CarAdvertController.updateCarAdvert().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          testAdvertUsedCar1Json))

      status(updateCallResult) mustBe 200
      contentAsString(updateCallResult) mustBe testAdvertUsedCar1Json
    }

    "list both used and new car adverts" in {
      object carAdvertController extends CarAdvertControllerComponent
        with CarAdvertRepositoryComponent
        with TestAppConfiguration {
        val repositoryMock = mock[CarAdvertRepository]
        (repositoryMock.getAll _).expects().returning(Seq(testAdvert1, testAdvertUsedCar1))
        val carAdvertRepository = repositoryMock
      }

      val listingCallResult = call(carAdvertController.listAllAdverts(None, None), FakeRequest())

      status(listingCallResult) mustBe 200
      contentAsString(listingCallResult) mustBe s"[$testAdvert1Json,$testAdvertUsedCar1Json]"
    }


    "validate a used car advert without required fields" in {

      object carAdvertController extends CarAdvertControllerComponent
        with CarAdvertRepositoryComponent
        with TestAppConfiguration {
        val carAdvertRepository = mock[CarAdvertRepository]
      }

      val invalidUsedCarAdvertJsonNoRequiredFields = """{"id":"","title":"ZAZ-968","price":5,"fuelType":"gasoline",""" +
        """"isNew":false}"""

      val invalidUsedCarAdvertJsonDateFormatBroken = """{"id":"","title":"ZAZ-968","price":5,"fuelType":"gasoline",""" +
        """"isNew":false,"mileage":100000,"firstRegistrationDate":"25-12-1900"}"""


      val creationCallResult1 = call(carAdvertController.createCarAdvert,
        FakeRequest(
          Helpers.POST,
          controllers.routes.CarAdvertController.createCarAdvert().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          invalidUsedCarAdvertJsonNoRequiredFields))
      status(creationCallResult1) mustBe 406
      contentAsString(creationCallResult1) mustBe """[{"path":"mileage","errorDescription":"Mileage must be present for""" +
        """ used cars"},{"path":"firstRegistrationDate","errorDescription":"First registration date must be""" +
        """ present for used cars"}]"""

      val creationCallResult2 = call(carAdvertController.createCarAdvert,
        FakeRequest(
          Helpers.POST,
          controllers.routes.CarAdvertController.createCarAdvert().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          invalidUsedCarAdvertJsonDateFormatBroken))
      status(creationCallResult2) mustBe 406
      contentAsString(creationCallResult2) mustBe """[{"path":"obj.firstRegistrationDate","errorDescription":"cannot""" +
        """ parse the date, expected format yyyy-MM-ddTHH:mm:ssZ. Exception java.lang.IllegalArgumentException:""" +
        """ Invalid format: \"25-12-1900\" is malformed at \"00\""}]"""
    }

    "validate a car advert with unknown fuel type" in {

      object carAdvertController extends CarAdvertControllerComponent
        with CarAdvertRepositoryComponent
        with TestAppConfiguration {
        val carAdvertRepository = mock[CarAdvertRepository]
      }

      val invalidCarAdvertJsonIncorrectFuelType = """{"id":"","title":"ZAZ-968","price":5,"fuelType":"electricity",""" +
        """"isNew":true}"""



      val creationCallResult1 = call(carAdvertController.createCarAdvert,
        FakeRequest(
          Helpers.POST,
          controllers.routes.CarAdvertController.createCarAdvert().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          invalidCarAdvertJsonIncorrectFuelType))
      status(creationCallResult1) mustBe 406
      contentAsString(creationCallResult1) mustBe """[{"path":"obj.fuelType","errorDescription":"Unknown""" +
        """ fuel type electricity"}]"""

    }

    "list in ordered fashion" in {
      object carAdvertController extends CarAdvertControllerComponent
        with CarAdvertRepositoryComponent
        with TestAppConfiguration {
        val repositoryMock = mock[CarAdvertRepository]
        (repositoryMock.getAllSortedBy _).expects(where {
          (field: String, ascendingSort: Boolean) => field == "title" && ascendingSort
        }).returning(Seq(testAdvert1, testAdvertUsedCar1)).anyNumberOfTimes()
        (repositoryMock.getAllSortedBy _).expects(where {
          (field: String, ascendingSort: Boolean) => field == "title" && !ascendingSort
        }).returning(Seq(testAdvertUsedCar1, testAdvert1)).anyNumberOfTimes()
        val carAdvertRepository = repositoryMock
      }

      val listingCallResult1 = call(carAdvertController.listAllAdverts(Some("title"), None), FakeRequest())
      contentAsString(listingCallResult1) mustBe s"[$testAdvertUsedCar1Json,$testAdvert1Json]"

      val listingCallResult2 = call(carAdvertController.listAllAdverts(Some("title"), Some("true")), FakeRequest())
      contentAsString(listingCallResult2) mustBe s"[$testAdvert1Json,$testAdvertUsedCar1Json]"

      val listingCallResult3 = call(carAdvertController.listAllAdverts(Some("title"), Some("false")), FakeRequest())
      contentAsString(listingCallResult3) mustBe s"[$testAdvertUsedCar1Json,$testAdvert1Json]"
    }
  }
}

