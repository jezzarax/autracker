# Car adverts tracking API
 
## [Problem description](file://TestBackendScala.md)


## Installation&running

### Local

- Prerequisites 

	- JDK 1.7+
	- SBT
	- Unoccupied port 9000
	- MongoDB 2.6+

- Launching the API
    
    - Provide an existing database on MongoDB server 
	- Update conf/application.conf to point to the correct MongoDB instance
	    - It is pointing to the autracking database on local instance of MongoDB
	- sbt run
	- point your rest client onto http://localhost:9000/

- Running the tests

    - Provide an existing database on MongoDB server
	- Update connection strings in TestAppConfiguration tests/RepositorySpec.scala to point to the correct MongoDB instance if needed
	    - It is pointing to the autracker_tests database on local instance of MongoDB
	- sbt test

### Inside VM

- Prerequisites

	- Oracle virtualbox 5.0+
	- Vagrant
		- Compatible with your virtualbox version
	- Rsync
	- SSH
	- Port 9000 free

- Running the API

	- vagrant up
	- point your rest client onto http://localhost:9000/

## Architectural challenges and considerations

### Code structure
    
- Naked domain model is used due to the simplicity of the task
  - app\domain folder contains domain types

- Repository class wrapped with cake DI pattern is used

- Controller class is injected with the repository directly and contains 
    the validation and error handling logic due to  simplicity of the 
    requirements so introduction of the separate service layer could cause 
    unnecessary increase of complexity


### Testing

- No test pyramid
	- Only technical requirements were provided, so acceptance tests would require redefining business value with the end customer
- Tests are split into two parts
    - RepositorySpec tests that cover the repository behaviour and affect the test database
    - ContollerSpec tests cover controller and use repository mock to simplify SUT setup
    

### Data contract format

- **CarAdvert** data contract is expected to have the following fields
    - **id** maps to **id** in the problem description
    - **title** maps to **title** in the problem description
    - **fuelType** maps to **fuel** in the problem description
    - **price** maps to **price** in the problem description
    - **isNew** maps to **new** in the problem description
    - **mileage** maps to **mileage** in the problem description
    - **firstRegistrationDate** maps to **first registration** in the problem description
- the fields presence requirements conforms the problem description
- due to the simplicity of data contract **mileage** and 
**firstRegistrationDate** were left in the **CarAdvert** data contract 
for both cases of new and used cars and the logic of these cases 
handling was put in both controller json parsing and document hanlding 
in repository. In case of increasing complexity, splitting the CarAdvert 
case class into separate classes (with abstract class or trait) should 
be considered. 

- Actions return values
    - car adverts listing returns the json list of CarAdvert entities
    - single car advert listing returns the json representation CarAdvert entity
    - successful insertion of car advert entity returns the same entity with the id set and HTTP code **Created**
    - successful update of car advert returns the updated entity back and HTTP code **OK**
    - successful deletion of car advert returns the deleted advert id and HTTP code **OK**
    
- Error responses
    - Validation errors can be produced by **UPDATE** and **INSERT** actions and 
    represented by a list of pairs of erroneous json path and validation error message
    - **DELETE** action can return a 404 error in case the CarAdvert 
    with the id is not presented in the database

- RESTful standards conformance
    - Date format choice ideas
        - JSON standard does not have date format defined
        - Microsoft .NET date format is the most common option in the 
        .NET stack world but due to incompatibilities with all other 
        stacks is a bad choice
        - UNIX time is a great choice when the precision is required
        - in our case only day-level precision is needed so ISO-8601 
        compatible format was chosen. The valid example is
         __1976-12-25T00:00:00+0100__
            
    - PUT&DELETE verbs idempotency
        - the idempotency requirements are not strictly defined in the 
        RESTful agreements and recommendations and understanding of the 
        verbs meaning varies as we can see in megabytes of discussions 
        on stackoverflow and reddit.
        - In this implementation **PUT** always succeeds if the 
        validation succeeds. **DELETE** fails if the id is not found. 
    
     

### DI mechanism choice

- Dependency injection is implemented using the cake pattern with a 
class that is injected into is wrapped with a component trait and 
self-type annotation used as an injection target.

- Other options are also available but were not used do to simplicity 
and toy-scale of the defined problem.

### Database access

- the database timeout is hardcoded and equals to 10 seconds. It can be 
exposed into the configuration file and/or retry mechanism can be 
implemented if performance would be an issue


